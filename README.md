# Git Practice

For practicing the use of Git, you are assigned to do these following tasks:

* Create a new branch with your name as the branch name.
* Create a new file with <your name>.txt as the file name, and write some text in it.
* Push the created file to your branch.
* Create a Merge Request with your branch.
